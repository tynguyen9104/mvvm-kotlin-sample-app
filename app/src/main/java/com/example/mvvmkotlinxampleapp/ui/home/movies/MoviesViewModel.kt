package com.example.mvvmkotlinxampleapp.ui.home.movies

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.data.db.movies_entities.Movie
import com.example.mvvmkotlinxampleapp.util.lazyDeferred
import kotlinx.coroutines.Deferred

class MoviesViewModel(private val repository: MoviesRepository) : ViewModel() {
    val movies by lazyDeferred {
        Log.d("Vip", ":dsfsdfdsafsdaf ")
        repository.getMovies()
    }

    suspend fun getMovies(): LiveData<List<Movie>> {
       return repository.getMoviesWithRefreshLayout()
    }

}