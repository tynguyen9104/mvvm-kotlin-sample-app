package com.example.mvvmkotlinxampleapp.ui.home.movies

import android.view.View
import com.example.mvvmkotlinxampleapp.data.db.movies_entities.Movie

interface RrcycleclerViewClickListener {
    fun onRecyclerViewItemOnclick(view: View, movie: Movie)
}