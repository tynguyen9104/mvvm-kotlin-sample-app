package com.example.mvvmkotlinxampleapp.ui.home.quotes

import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.util.lazyDeferred

class QuotesViewMode(
    repository: QuotesRespository
) : ViewModel() {

    val quotes by lazyDeferred { repository.getQuotes() }

}