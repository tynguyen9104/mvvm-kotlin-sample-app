package com.example.mvvmkotlinxampleapp.ui.authenticaiton

import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.data.db.entities.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AutheViewModel(private val repository: UserRepository) : ViewModel() {
    fun getLoggedInUser() = repository.getUser()

    suspend fun userLogin(
        email: String,
        password: String
    ) = withContext(Dispatchers.IO){repository.userLogin(email, password)}

    suspend fun saveLoggedInUser(user: User) = repository.saveUser(user)

    suspend fun userSignup(
        name: String,
        email: String,
        password: String
    ) = withContext(Dispatchers.IO){repository.userSignup(name, email, password)}
}



