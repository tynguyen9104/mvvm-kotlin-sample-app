package com.example.mvvmkotlinxampleapp.ui.home.quotes

import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.data.db.entities.Quote
import com.example.mvvmkotlinxampleapp.databinding.ItemQuoteBinding
import com.xwray.groupie.databinding.BindableItem

class QuoteItem(
    private val quote:Quote
) :BindableItem<ItemQuoteBinding>(){
    override fun getLayout() = R.layout.item_quote

    override fun bind(viewBinding: ItemQuoteBinding, position: Int) {
        viewBinding.setQuote(quote)
    }


}