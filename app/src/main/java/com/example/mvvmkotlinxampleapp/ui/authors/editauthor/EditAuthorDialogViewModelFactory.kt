package com.example.mvvmkotlinxampleapp.ui.authors.editauthor

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class EditAuthorDialogViewModelFactory() : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EditAuthorDialogViewModel() as T
    }
}
