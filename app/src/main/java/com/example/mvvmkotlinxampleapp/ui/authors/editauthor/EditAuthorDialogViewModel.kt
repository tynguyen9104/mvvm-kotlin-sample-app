package com.example.mvvmkotlinxampleapp.ui.authors.editauthor

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author
import com.example.mvvmkotlinxampleapp.util.Constants
import com.google.firebase.database.FirebaseDatabase
import java.lang.Exception

class EditAuthorDialogViewModel():ViewModel() {
    private val _result = MutableLiveData<Exception?>()
    val dbAuthors = FirebaseDatabase.getInstance().getReference(Constants.NODE_AUTHORS)
    val result: LiveData<Exception?>
        get() = _result

    fun updateAuthor(author: Author){

        dbAuthors.child(author.id!!).setValue(author).addOnCompleteListener {
            if (it.isSuccessful){
                _result.value == null
            }else{
                _result.value = it.exception
            }
        }
    }
}


