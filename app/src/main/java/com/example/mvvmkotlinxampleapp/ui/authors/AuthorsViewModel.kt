package com.example.mvvmkotlinxampleapp.ui.authors

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author
import com.example.mvvmkotlinxampleapp.util.Constants
import com.google.firebase.database.*
import java.lang.Exception

class AuthorsViewModel() : ViewModel() {
    val dbAuthors = FirebaseDatabase.getInstance().getReference(Constants.NODE_AUTHORS)
    private val _authors = MutableLiveData<List<Author>>()
    val authors: LiveData<List<Author>>
        get() = _authors

    private val _author = MutableLiveData<Author>()
    val author: LiveData<Author>
        get() = _author


    private val childEventListener = object : ChildEventListener {
        override fun onCancelled(error: DatabaseError) {
            TODO("Not yet implemented")
        }

        override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            TODO("Not yet implemented")
        }

        override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            val author = snapshot.getValue(Author::class.java)
            author?.id = snapshot.key
            _author.value = author
        }

        override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
            val author = snapshot.getValue(Author::class.java)
            author?.id = snapshot.key
            _author.value = author
        }

        override fun onChildRemoved(snapshot: DataSnapshot) {
            val author = snapshot.getValue(Author::class.java)
            author?.id = snapshot.key
            author?.isDeleted = true
            _author.value = author
        }
    }

    fun getRealtimeUpdates() {
        dbAuthors.addChildEventListener(childEventListener)
    }


    fun fetchAuthors() {
        dbAuthors.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val authors = mutableListOf<Author>()
                    for (authorSnapshot in snapshot.children) {
                        val author = authorSnapshot.getValue(Author::class.java)
                        author?.id = authorSnapshot.key
                        author?.let { authors.add(it) }
                    }
                    _authors.value = authors

                }
            }
        })
    }

    fun deleteAuthor(author: Author) {
        dbAuthors.child(author.id!!).setValue(null)
    }

    // this function is called when the Fragment is destroyed
    override fun onCleared() {
        super.onCleared()
        dbAuthors.removeEventListener(childEventListener)
    }
}