package com.example.mvvmkotlinxampleapp.ui.authors

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author
import kotlinx.android.synthetic.main.author_items.view.*

class AuthorsAdapter : RecyclerView.Adapter<AuthorsAdapter.AuthorViewModel>() {
    private var authors = ArrayList<Author>()
    var listener: AuthorClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = AuthorViewModel(
        LayoutInflater.from(parent.context).inflate(R.layout.author_items, parent, false)
    )

    override fun getItemCount() = authors.size

    override fun onBindViewHolder(holder: AuthorViewModel, position: Int) {
        holder.view.text_view_name.text = authors[position].name
        holder.view.button_delete.setOnClickListener {
            listener?.onRecyclerViewAuthorClick(it, authors[position])
        }
        holder.view.button_edit.setOnClickListener {
            listener?.onRecyclerViewAuthorClick(it, authors[position])
        }
    }

    fun setAuthors(authors: List<Author>) {
        this.authors = authors as ArrayList<Author>
        notifyDataSetChanged()
    }

    fun addAuthor(author: Author) {
        if (!authors.contains(author)) {
            authors.add(author)
        } else {
            val index = authors.indexOf(author)
            if (author.isDeleted) {
                authors.removeAt(index)
            } else {
                authors[index] = author
            }
        }
        notifyDataSetChanged()

    }

    class AuthorViewModel(val view: View) : RecyclerView.ViewHolder(view)
}