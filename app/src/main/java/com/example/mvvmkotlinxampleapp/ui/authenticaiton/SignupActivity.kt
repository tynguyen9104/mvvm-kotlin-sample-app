package com.example.mvvmkotlinxampleapp.ui.authenticaiton

import android.accounts.NetworkErrorException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.databinding.ActivitySignupBinding
import com.example.mvvmkotlinxampleapp.ui.home.HomeActivity
import com.example.mvvmkotlinxampleapp.util.ApiException
import com.example.mvvmkotlinxampleapp.util.snackbar
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SignupActivity : AppCompatActivity(), KodeinAware {
    private lateinit var binding: ActivitySignupBinding
    private lateinit var viewmodel: AutheViewModel
    override val kodein by kodein()
    private val factory by instance<AuthViewModelFactory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)

        viewmodel = ViewModelProvider(this, factory).get(AutheViewModel::class.java)

        viewmodel.getLoggedInUser().observe(this, Observer { user ->
            if (user != null) {
                Intent(this, HomeActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
        })

        binding.buttonSignUp.setOnClickListener {
            signupUser()
        }

        binding.textViewLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    private fun signupUser() {
        val name = binding.editTextName.text.toString().trim()
        val email = binding.editTextEmail.text.toString().trim()
        val password = binding.editTextPassword.text.toString().trim()
        val passwordConfirm = binding.editTextPasswordConfirm.text.toString().trim()

        if (name.isEmpty()) {
            binding.root.snackbar(message = "Name is required")
            return
        } else if (email.isEmpty()) {
            binding.root.snackbar(message = "Email is required")
            return
        } else if (password.isEmpty()) {
            binding.root.snackbar(message = "Password is required")
            return
        } else if (!password.equals(passwordConfirm)) {
            binding.root.snackbar(message = "Password did not match")
            return
        } else {
            try {
                lifecycleScope.launch {
                    val authResponse = viewmodel.userSignup(name, email, password)
                    if (authResponse.user != null) {
                        Log.d("Vip", "signupUser: " + authResponse.user)
                        viewmodel.saveLoggedInUser(authResponse.user)
                    } else {
                        binding.root.snackbar(authResponse.message!!)
                    }
                }

            } catch (e: NetworkErrorException) {
                e.printStackTrace()
            } catch (e: ApiException) {
                e.printStackTrace()
            }


        }
    }

}