package com.example.mvvmkotlinxampleapp.ui.home.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.data.db.movies_entities.Movie
import com.example.mvvmkotlinxampleapp.databinding.ItemMovieBinding


class MoviesAdapter(
    private var movies: ArrayList<Movie>,
    private val listener: RrcycleclerViewClickListener
) : RecyclerView.Adapter<MoviesAdapter.MoviewsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviewsViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_movie,
                parent, false
            )
        )

    override fun getItemCount() = movies.size

    override fun onBindViewHolder(holder: MoviewsViewHolder, position: Int) {
        holder.itemMovieBinding.movies = movies[position]
        holder.itemMovieBinding.buttonBook.setOnClickListener {
           listener.onRecyclerViewItemOnclick(holder.itemMovieBinding.buttonBook, movies[position])
        }

    }
    fun updateVideo(videoList:List<Movie>){
        this.movies = videoList as ArrayList<Movie>
        notifyDataSetChanged()
    }

    inner class MoviewsViewHolder(
        val itemMovieBinding: ItemMovieBinding
    ) : RecyclerView.ViewHolder(itemMovieBinding.root)

}