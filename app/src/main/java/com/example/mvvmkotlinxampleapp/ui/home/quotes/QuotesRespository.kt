package com.example.mvvmkotlinxampleapp.ui.home.quotes

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlinxampleapp.data.db.AppDatabase
import com.example.mvvmkotlinxampleapp.data.db.entities.Quote
import com.example.mvvmkotlinxampleapp.data.network.MyApi
import com.example.mvvmkotlinxampleapp.data.network.NetWorkConnectionIntercepter
import com.example.mvvmkotlinxampleapp.data.network.SafeApiRequest
import com.example.mvvmkotlinxampleapp.data.preferences.PreferenceProvider
import com.example.mvvmkotlinxampleapp.util.Common
import com.example.mvvmkotlinxampleapp.util.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class QuotesRespository(
    private val api: MyApi,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider,
    private val netWorkConnectionIntercepter: NetWorkConnectionIntercepter
) : SafeApiRequest() {

    private val quotes = MutableLiveData<List<Quote>>()

    init {
        quotes.observeForever() {
            saveQuotes(it)
            Log.d("Vip", ": asedfsdfdsf")
        }
    }

    suspend fun getQuotes(): LiveData<List<Quote>> {
        return withContext(Dispatchers.IO) {
            fetchQuotes()
            db.getQuoteDao().getQuotes()
        }
    }

    private suspend fun fetchQuotes() {
        val lastSaveQuoteAt = prefs.getLastSaveQuotedAt()
        if (lastSaveQuoteAt == null
            || (isFetchNeeded(lastSaveQuoteAt.toLong()) && netWorkConnectionIntercepter.isInternetAvailable())
        ) {
            val response = apiRequest { api.getQuotes() }
            quotes.postValue(response.quotes)
        } else {
            return
        }

    }

    private fun isFetchNeeded(savedAt: Long): Boolean {
        return savedAt + Common.TIME_TO_FETCH_DATA < System.currentTimeMillis()
    }

    private fun saveQuotes(quotes: List<Quote>) {
        Coroutines.io {
            prefs.savelastSaveQuoteAt(System.currentTimeMillis().toString())
            db.getQuoteDao().saveAllQuotes(quotes)
        }
    }
}


