package com.example.mvvmkotlinxampleapp.ui.home.profile

import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.ui.authenticaiton.UserRepository

class ProfileViewModel (userRepository: UserRepository): ViewModel() {

    val user = userRepository.getUser()

}