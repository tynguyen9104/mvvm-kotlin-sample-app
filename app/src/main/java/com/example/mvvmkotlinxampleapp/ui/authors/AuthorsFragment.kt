package com.example.mvvmkotlinxampleapp.ui.authors

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.databinding.AuthorsFragmentBinding
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author
import com.example.mvvmkotlinxampleapp.ui.authors.editauthor.EditAuthorDialogFragment
import kotlinx.android.synthetic.main.authors_fragment.*
import kotlinx.android.synthetic.main.authors_fragment.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AuthorsFragment() : Fragment(), KodeinAware, AuthorClickListener {
    override val kodein by kodein()
    private val factory by instance<AuthorsViewModelFactory>()
    private val adapter = AuthorsAdapter()


    private lateinit var viewModel: AuthorsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: AuthorsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.authors_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(AuthorsViewModel::class.java)

        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        binding.root.button_add.setOnClickListener {
            findNavController().navigate(R.id.action_authorsFragment_to_addAuthorDialogFragment)
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler_view_authors.adapter = adapter
        adapter.listener = this
        viewModel.fetchAuthors()
        viewModel.getRealtimeUpdates()
        viewModel.authors.observe(viewLifecycleOwner, Observer {
            adapter.setAuthors(it)
        })
        viewModel.author.observe(viewLifecycleOwner, Observer {
            adapter.addAuthor(it)
        })
    }

    override fun onRecyclerViewAuthorClick(view: View, author: Author) {
        when (view.id) {
            R.id.button_edit -> {
                Log.d("Vip", "onRecyclerViewAuthorClick: "+author.name)
                EditAuthorDialogFragment(author).show(childFragmentManager,"")
            }
            R.id.button_delete -> {
                AlertDialog.Builder(requireContext()).also {
                    it.setTitle(getString(R.string.delete_confirmation))
                    it.setPositiveButton(getString(R.string.yes)){dialogInterface, i ->
                        viewModel.deleteAuthor(author)
                    }
                }.create().show()

            }
        }
    }
}