package com.example.mvvmkotlinxampleapp.ui.authors.addauthor

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class AddAuthorDialogViewModelFactory() : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddAuthorDialogViewModel() as T
    }

}
