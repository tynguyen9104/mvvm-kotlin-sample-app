package com.example.mvvmkotlinxampleapp.ui.home.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.databinding.ProfileFragmentBinding
import org.kodein.di.android.x.kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class ProfileFragment : Fragment(), KodeinAware {
    override val kodein by kodein()
    private lateinit var viewModel: ProfileViewModel
    private val factory by instance<ProfileViewModelFactory>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val binding: ProfileFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        binding.btnMvvmRetrofitRecyclerview.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_moviesFragment)
//            crashNow()
        }
        binding.btnMvvmFirebaseRecyclerview.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_authorsFragment)
        }

        return binding.root
    }

    private fun crashNow() {
        throw RuntimeException("App Crashed")
    }


}