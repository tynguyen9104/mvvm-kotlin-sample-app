package com.example.mvvmkotlinxampleapp.ui.home.movies_details

import com.example.mvvmkotlinxampleapp.data.db.AppDatabase
import com.example.mvvmkotlinxampleapp.data.network.MoviesApi
import com.example.mvvmkotlinxampleapp.data.preferences.PreferenceProvider

class MovieDetailRepository(private val api: MoviesApi,
                            private val db: AppDatabase,
                            private val prefs: PreferenceProvider) {
}