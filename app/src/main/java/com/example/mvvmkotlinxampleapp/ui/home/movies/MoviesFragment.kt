package com.example.mvvmkotlinxampleapp.ui.home.movies

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.data.db.movies_entities.Movie
import com.example.mvvmkotlinxampleapp.util.Coroutines
import com.example.mvvmkotlinxampleapp.util.hide
import com.example.mvvmkotlinxampleapp.util.show
import kotlinx.android.synthetic.main.movies_fragment.*
import kotlinx.android.synthetic.main.movies_fragment.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class MoviesFragment : Fragment(), RrcycleclerViewClickListener, KodeinAware {
    override val kodein by kodein()
    private val factory by instance<MoviesViewModelFactory>()
    private lateinit var viewModel: MoviesViewModel
    private lateinit var movies: List<Movie>
    private lateinit var mAdapter: MoviesAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.movies_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(MoviesViewModel::class.java)
        movies = ArrayList<Movie>()
        mAdapter = MoviesAdapter(movies as ArrayList<Movie>, this)

        view.recycler_view_movies.also {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.setHasFixedSize(true)
            it.adapter = mAdapter
        }
        updateVideoUI()
        view.refreshLayout.setOnRefreshListener {
            progress_bar_movies.show()
            refreshLayout.isRefreshing = true
            Coroutines.main {
                val movies = viewModel.getMovies()
                if (view != null) {
                    movies.value?.let { mAdapter.updateVideo(it) }
                }
            }
            refreshLayout.isRefreshing = false
        }
        return view
    }

    fun updateVideoUI() {
        Coroutines.main {
            progress_bar_movies.show()
            refreshLayout.isRefreshing = true
            val movies = viewModel.movies.await()
            if (view != null) {
                movies.observe(viewLifecycleOwner, Observer { movieList ->
                    progress_bar_movies.hide()
                    mAdapter.updateVideo(movieList)
                    refreshLayout.isRefreshing = false
                })
            }
        }

    }

    override fun onRecyclerViewItemOnclick(view: View, movie: Movie) {
        when (view.id) {
            R.id.button_book -> {
                findNavController().navigate(R.id.action_moviesFragment_to_movieDetailFragment)
            }
        }
    }

}