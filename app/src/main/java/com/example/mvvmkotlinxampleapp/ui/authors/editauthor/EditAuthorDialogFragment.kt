package com.example.mvvmkotlinxampleapp.ui.authors.editauthor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author
import kotlinx.android.synthetic.main.author_items.*
import kotlinx.android.synthetic.main.dialog_fragment_add_authors.*
import kotlinx.android.synthetic.main.dialog_fragment_add_authors.edit_text_name
import kotlinx.android.synthetic.main.dialog_fragment_add_authors.input_layout_name
import kotlinx.android.synthetic.main.dialog_fragment_edit_authors.*
import kotlinx.android.synthetic.main.item_quote.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class EditAuthorDialogFragment(private val author: Author) : DialogFragment(), KodeinAware {
    override val kodein by kodein()
    private lateinit var viewModel: EditAuthorDialogViewModel
    private val factory by instance<EditAuthorDialogViewModelFactory>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this, factory).get(EditAuthorDialogViewModel::class.java)
        return inflater.inflate(R.layout.dialog_fragment_edit_authors, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        edit_text_name.setText(author.name)
        button_edit_from_dialog.setOnClickListener {
            val name = edit_text_name.text.toString().trim()
            if (name.isEmpty()) {
                input_layout_name.error = getString(R.string.error_field_required)
                return@setOnClickListener
            }
            author.name = name
            viewModel.updateAuthor(author)
            dismiss()
        }
    }

}