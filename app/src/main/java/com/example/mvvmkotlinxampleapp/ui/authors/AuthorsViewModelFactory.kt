package com.example.mvvmkotlinxampleapp.ui.authors

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmkotlinxampleapp.ui.home.movies.MoviesRepository
import com.example.mvvmkotlinxampleapp.ui.home.movies.MoviesViewModel

@Suppress("UNCHECKED_CAST")
class AuthorsViewModelFactory() :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AuthorsViewModel() as T
    }
}
