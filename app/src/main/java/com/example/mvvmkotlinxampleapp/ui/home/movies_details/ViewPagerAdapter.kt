package com.example.mvvmkotlinxampleapp.ui.home.movies_details

import android.os.Parcel
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(
    private var CARD_ITEM_SIZE: Int,
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return CARD_ITEM_SIZE
    }


    override fun createFragment(position: Int): Fragment {
        return FragmentViewPager.newInstance(position)
    }


}