package com.example.mvvmkotlinxampleapp.ui.authenticaiton

import com.example.mvvmkotlinxampleapp.data.db.AppDatabase
import com.example.mvvmkotlinxampleapp.data.db.entities.User
import com.example.mvvmkotlinxampleapp.data.network.MyApi
import com.example.mvvmkotlinxampleapp.data.network.SafeApiRequest
import com.example.mvvmkotlinxampleapp.data.network.responses.AuthResponse

class UserRepository(private val api: MyApi, private val db: AppDatabase) : SafeApiRequest() {

    suspend fun userLogin(email: String, password: String): AuthResponse {
        return apiRequest { api.userLogin(email, password) }
    }
    suspend fun userSignup(name: String, email: String, password: String):AuthResponse{
        return apiRequest { api.userSignup(name,email,password) }
    }

    suspend fun saveUser(user: User) = db.getUserDao().upSert(user)


    fun getUser() = db.getUserDao().getUser()

}