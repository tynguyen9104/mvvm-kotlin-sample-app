package com.example.mvvmkotlinxampleapp.ui.home.movies_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.mvvmkotlinxampleapp.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator;
import kotlinx.android.synthetic.main.movie_detail_fragment.*
import kotlinx.android.synthetic.main.movie_detail_fragment.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class MovieDetailFragment : Fragment(), KodeinAware {
    override val kodein by kodein()
    private lateinit var viewModel: MovieDetailViewModel
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager2: ViewPager2
    val list: List<String> = listOf("tynguyen", "thivan", "abc")
    private val factory by instance<MovieDetailViewModelFactory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.movie_detail_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(MovieDetailViewModel::class.java)
        viewPager2 = view.findViewById(R.id.view_pager_2)
        tabLayout = view.findViewById(R.id.tab_layout)
//        viewPager2.adapter = ViewPagerAdapter(3, requireActivity().supportFragmentManager,lifecycle)
        viewPager2.adapter = ViewPagerAdapter(3,childFragmentManager,lifecycle)

        TabLayoutMediator(
            tabLayout,
            viewPager2,
            object : TabLayoutMediator.TabConfigurationStrategy {
                override fun onConfigureTab(tab: TabLayout.Tab, position: Int) {
                       tab.text = list.get(position)
                }
            }).attach()

        return view
    }

}