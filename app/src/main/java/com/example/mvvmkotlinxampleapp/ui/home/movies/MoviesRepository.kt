package com.example.mvvmkotlinxampleapp.ui.home.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlinxampleapp.data.db.AppDatabase
import com.example.mvvmkotlinxampleapp.data.db.movies_entities.Movie
import com.example.mvvmkotlinxampleapp.data.network.MoviesApi
import com.example.mvvmkotlinxampleapp.data.network.NetWorkConnectionIntercepter
import com.example.mvvmkotlinxampleapp.data.network.SafeApiRequest
import com.example.mvvmkotlinxampleapp.data.preferences.PreferenceProvider
import com.example.mvvmkotlinxampleapp.util.Common
import com.example.mvvmkotlinxampleapp.util.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MoviesRepository(
    private val api: MoviesApi,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider,
    private val netWorkConnectionIntercepter: NetWorkConnectionIntercepter) : SafeApiRequest() {
    private var movies = MutableLiveData<List<Movie>>()

    init {
        movies.observeForever {
            saveMovies(it)
        }
    }

        suspend fun getMovies(): LiveData<List<Movie>> {
            return withContext(Dispatchers.IO) {
                fetchMovies()
                db.getMovieDao().getMovies()
            }
        }
    suspend fun getMoviesWithRefreshLayout(): LiveData<List<Movie>> {
        return withContext(Dispatchers.IO) {
            fetchMoviesWithRefreshLayout()
            db.getMovieDao().getMovies()
        }
    }

    private suspend fun fetchMovies() {
        val lastSaveVideoAt = prefs.getLastSaveMoviedAt()
        if (netWorkConnectionIntercepter.isInternetAvailable()) {
            if ((lastSaveVideoAt == null)
                || (isFetchNeeded(lastSaveVideoAt.toLong()))
            ) {
                movies.postValue(apiRequest { api.getMovies() })
            }
        }
    }

        private suspend fun fetchMoviesWithRefreshLayout() {
            if (netWorkConnectionIntercepter.isInternetAvailable()){
                movies.postValue(apiRequest { api.getMovies() })
            }

        }


    private fun isFetchNeeded(saveAt: Long): Boolean {
        return saveAt + Common.TIME_TO_FETCH_DATA < System.currentTimeMillis()
    }

    private fun saveMovies(movies: List<Movie>?) {
        Coroutines.io {
            if (movies != null) {
                db.getMovieDao().deleteAllMovies()
                db.getMovieDao().saveAllMovies(movies)
                prefs.savelastSaveMovieAt(System.currentTimeMillis().toString())
            }
        }
    } }