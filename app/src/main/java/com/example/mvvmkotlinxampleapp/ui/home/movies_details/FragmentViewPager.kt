package com.example.mvvmkotlinxampleapp.ui.home.movies_details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mvvmkotlinxampleapp.R


/**
 * A simple [Fragment] subclass.
 * Use the [FragmentViewPager.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentViewPager : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_view_pager, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(counter: Int) =
            FragmentViewPager().apply {

            }
    }
}