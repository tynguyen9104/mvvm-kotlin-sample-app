package com.example.mvvmkotlinxampleapp.ui.authors

import android.view.View
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author

interface AuthorClickListener {
    fun onRecyclerViewAuthorClick(view: View, author: Author)
}