package com.example.mvvmkotlinxampleapp.ui.home.quotes

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.data.db.entities.Quote
import com.example.mvvmkotlinxampleapp.databinding.QuotesFragmentBinding
import com.example.mvvmkotlinxampleapp.util.Coroutines
import com.example.mvvmkotlinxampleapp.util.hide
import com.example.mvvmkotlinxampleapp.util.show
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.quotes_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class QuotesFragment : Fragment(), KodeinAware {

    override val kodein by kodein()
    private val factory by instance<QuotesViewModelFactory>()

    private lateinit var viewModel: QuotesViewMode
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: QuotesFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.quotes_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(QuotesViewMode::class.java)
        bindUI()
    }

    private fun bindUI() = Coroutines.main {
        progress_bar.show()
        val quotes = viewModel.quotes.await()
        if (view != null) {
            quotes.observe(viewLifecycleOwner, Observer {
                progress_bar.hide()
                initRecyclerView(it.toQuoteItem())
            })
        }

    }

    private fun initRecyclerView(quoteItems: List<QuoteItem>) {
        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(quoteItems)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    private fun List<Quote>.toQuoteItem(): List<QuoteItem> {
        return this.map {
            QuoteItem(it)
        }
    }

}