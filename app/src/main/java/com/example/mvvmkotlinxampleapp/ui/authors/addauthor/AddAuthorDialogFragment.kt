package com.example.mvvmkotlinxampleapp.ui.authors.addauthor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmkotlinxampleapp.R
import com.example.mvvmkotlinxampleapp.ui.authors.data.Author
import kotlinx.android.synthetic.main.dialog_fragment_add_authors.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class AddAuthorDialogFragment : DialogFragment(), KodeinAware {
    override val kodein by kodein()
    private lateinit var viewModel: AddAuthorDialogViewModel
    private val factory by instance<AddAuthorDialogViewModelFactory>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this, factory).get(AddAuthorDialogViewModel::class.java)
        return inflater.inflate(R.layout.dialog_fragment_add_authors, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.result.observe(viewLifecycleOwner, Observer {
            val massage = if (it == null) {
                getString(R.string.author_added)
            } else {
                getString(R.string.author_error,it.message)
            }
            Toast.makeText(requireContext(),massage,Toast.LENGTH_SHORT).show()
        })

        button_add.setOnClickListener {
            val name = edit_text_name.text.toString().trim()
            if (name.isEmpty()) {
                input_layout_name.error = getString(R.string.error_field_required)
                return@setOnClickListener
            }
            val author = Author()
            author.name = name
            viewModel.addAuthor(author)
            dismiss()
        }
    }

}