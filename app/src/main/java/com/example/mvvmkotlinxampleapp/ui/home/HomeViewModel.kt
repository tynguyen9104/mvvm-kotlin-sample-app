package com.example.mvvmkotlinxampleapp.ui.home

import androidx.lifecycle.ViewModel
import com.example.mvvmkotlinxampleapp.ui.authenticaiton.UserRepository

class HomeViewModel(private val repository: UserRepository): ViewModel() {
    fun getLoggedInUser() = repository.getUser()

}