package com.example.mvvmkotlinxampleapp

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.example.mvvmkotlinxampleapp.data.db.AppDatabase
import com.example.mvvmkotlinxampleapp.data.network.MoviesApi
import com.example.mvvmkotlinxampleapp.data.network.MyApi
import com.example.mvvmkotlinxampleapp.data.network.NetWorkConnectionIntercepter
import com.example.mvvmkotlinxampleapp.data.preferences.PreferenceProvider
import com.example.mvvmkotlinxampleapp.ui.home.movies.MoviesRepository
import com.example.mvvmkotlinxampleapp.ui.home.quotes.QuotesRespository
import com.example.mvvmkotlinxampleapp.ui.authenticaiton.UserRepository
import com.example.mvvmkotlinxampleapp.ui.authenticaiton.AuthViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.authors.AuthorsRepository
import com.example.mvvmkotlinxampleapp.ui.authors.AuthorsViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.authors.addauthor.AddAuthorDialogViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.authors.editauthor.EditAuthorDialogViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.home.HomeViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.home.movies.MoviesViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.home.movies_details.MovieDetailRepository
import com.example.mvvmkotlinxampleapp.ui.home.movies_details.MovieDetailViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.home.profile.ProfileViewModelFactory
import com.example.mvvmkotlinxampleapp.ui.home.quotes.QuotesViewModelFactory
import com.example.mvvmkotlinxampleapp.util.isNight
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule

import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

// this class is instantiated before anything else
class MVVMApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MVVMApplication))

        bind() from singleton {
            NetWorkConnectionIntercepter(instance())
        }

        bind() from singleton {
            MyApi(instance())
        }

        bind() from singleton {
            MoviesApi(instance())
        }

        bind() from singleton {
            AppDatabase(instance())
        }

        bind() from singleton { PreferenceProvider(instance()) }

        bind() from singleton {
            UserRepository(
                instance(),
                instance()
            )
        }
        bind() from singleton {
            QuotesRespository(
                instance(),
                instance(),
                instance(),
                instance()
            )
        }
        bind() from singleton {
            MoviesRepository(
                instance(),
                instance(),
                instance(),
                instance()
            )
        }

        bind() from singleton {
            AuthorsRepository()
        }

        bind() from singleton { MovieDetailRepository(instance(), instance(), instance()) }

        bind() from provider {
            AuthViewModelFactory(instance())
        }

        bind() from provider {
            ProfileViewModelFactory(instance())
        }

        bind() from provider {
            QuotesViewModelFactory(instance())
        }

        bind() from provider {
            MoviesViewModelFactory(instance())
        }

        bind() from provider {
            HomeViewModelFactory(instance())
        }

        bind() from provider {
            MovieDetailViewModelFactory(
                instance()
            )
        }
        bind() from provider {
            AddAuthorDialogViewModelFactory()
        }
        bind() from provider {
            AuthorsViewModelFactory()
        }
        bind() from provider {
            EditAuthorDialogViewModelFactory()
        }

    }

    override fun onCreate() {
        super.onCreate()
        // Get UI mode and set
        val mode = if (isNight()) {
            AppCompatDelegate.MODE_NIGHT_YES
        } else {
            AppCompatDelegate.MODE_NIGHT_NO
        }
        AppCompatDelegate.setDefaultNightMode(mode)
    }
}