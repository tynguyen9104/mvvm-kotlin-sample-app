package com.example.mvvmkotlinxampleapp.data.network.responses

import com.example.mvvmkotlinxampleapp.data.db.entities.Quote

data class QuotesResponse(
    val isSuccessful: Boolean,
    val quotes: List<Quote>
)