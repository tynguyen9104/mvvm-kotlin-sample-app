package com.example.mvvmkotlinxampleapp.data.network.responses

import com.example.mvvmkotlinxampleapp.data.db.entities.User

data class AuthResponse (
    val isSuccessful: Boolean?,
    val message: String?,
    val user: User
)