package com.example.mvvmkotlinxampleapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mvvmkotlinxampleapp.data.db.entities.CURENT_USER_ID
import com.example.mvvmkotlinxampleapp.data.db.entities.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upSert(user: User): Long

    @Query("SELECT * FROM user WHERE uid = $CURENT_USER_ID")
    fun getUser(): LiveData<User>
}