package com.example.mvvmkotlinxampleapp.data.network

import com.example.mvvmkotlinxampleapp.data.db.movies_entities.Movie
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MoviesApi {
    @GET("2013625/raw")
    suspend fun getMovies(): Response<List<Movie>>

    companion object {
        operator fun invoke(netWorkConnectionIntercepter: NetWorkConnectionIntercepter): MoviesApi {
            val okHttpClient =
                OkHttpClient.Builder().addInterceptor(netWorkConnectionIntercepter).build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://gitlab.com/-/snippets/")
                .build()
                .create(MoviesApi::class.java)
        }
    }
}