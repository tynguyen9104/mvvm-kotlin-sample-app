package com.example.mvvmkotlinxampleapp.data.db.movies_entities

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mvvmkotlinxampleapp.data.db.entities.Quote

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllMovies(movies: List<Movie>)

    @Query("SELECT * FROM Movie")
    fun getMovies(): LiveData<List<Movie>>
    @Query("DELETE FROM MOVIE")
    fun deleteAllMovies()
}