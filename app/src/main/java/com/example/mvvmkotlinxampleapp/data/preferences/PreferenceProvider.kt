package com.example.mvvmkotlinxampleapp.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

private const val QUOTES_KEY_SAVED_AT = "quotes_key_save_at"
private const val MOVIES_KEY_SAVED_AT = "movies_key_save_at"

class PreferenceProvider(
    context: Context
) {
    private val appContext = context.applicationContext
    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)

    fun savelastSaveQuoteAt(saveAt: String) {
        preference.edit().putString(QUOTES_KEY_SAVED_AT, saveAt).apply()
    }

    fun getLastSaveQuotedAt(): String? {
        return preference.getString(QUOTES_KEY_SAVED_AT, null)
    }

    fun savelastSaveMovieAt(saveAt: String) {
        preference.edit().putString(MOVIES_KEY_SAVED_AT, saveAt).apply()
    }

    fun getLastSaveMoviedAt(): String? {
        return preference.getString(MOVIES_KEY_SAVED_AT, null)
    }

}